﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using RemoteConfigurations;

[CreateAssetMenu]
public class IntervalValues : Vector2Variable
{
    private void OnEnable()
    {
        /* 
        Here we can make use of RemoteConfigurations in order to pull 
        the values from the cloud and have available the flexibility
        to change parameters without submitting a new update to the store.
        int yLimitCloud = RemoteConfigurations.GetInt("xMinimumValue", xDefaultValue);
        int xLimitCloud = RemoteConfigurations.GetInt("yMinimumValue", yDefaultValue);
        Value = new Vector2(xLimitCloud, yLimitCloud);
        */

        Value = new Vector2(Value.x, Value.y);
    }
}
