﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Vector2Variable : ScriptableObject
{
    //encapsulation of a Vector2 value in order to use it
    //in different other scripts
    public Vector2 Value;
}
