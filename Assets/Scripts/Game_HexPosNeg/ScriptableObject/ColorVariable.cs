﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class ColorVariable : ScriptableObject
{
    //encapsulation of a Color value in order to use it
    //in different other scripts
    public Color Value;
}
