﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu]
public class BoardSize : ScriptableObject
{

    public int Rows;
    public int Columns;
}
