﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Games.HexPosNeg
{
    public class Score
    {
        //-------------- Members ----------------
        public delegate void ScoreChangedDelegate(int value);
        public ScoreChangedDelegate OnScoreChange;
        private int m_Score;
        private int m_MaxScore;


        //-------------- Methods ----------------

        public Score(IntVariable maxScore)
        {
            MaxScore = maxScore.Value;
        }

        public int Increase
        {
            set
            {
                m_Score += value;
                if (OnScoreChange != null)
                {
                    OnScoreChange(value);
                }
            }
        }
        public int Value
        {
            get { return m_Score; }
            set { m_Score = value; }
        }

        public int MaxScore
        {
            get { return m_MaxScore; }
            set { m_MaxScore = value; }
        }
    }
}