﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// References to Cube Coordinates Implementations and algorithms
/// http://www.redblobgames.com/grids/hexagons/
/// https://www.youtube.com/channel/UCPXOQq7PWh5OdCwEO60Y8jQ
/// https://gamedev.stackexchange.com/questions/116035/finding-cells-within-range-on-hexagonal-grid
/// http://keekerdc.com/2011/03/hexagon-grids-coordinate-systems-and-distance-calculations/
/// </summary>

public class CubicCoordinates
{
    //---------------------- Member Variables ---------------------

    //Cubic Coordinates values
    public readonly int X; // X for rows
    public readonly int Y; // Y for columns
    public readonly int Z;

    //Multipliers used to scale the positions of the hexagons, hor, ver or global
    readonly float WidthMultiplier = Mathf.Sqrt(3) / 2;
    readonly float HeightMultiplier = 0.717f;
    readonly float Multiplier = 1.1f;

    //Values used to calculate the world position
    float m_Radius = 1;
    float m_Height;
    float m_Width;
    float m_Horizontal_spacing;
    float m_Vertical_spacing;
    Vector3 m_Position;

    //-------------- Methods ----------------

    public CubicCoordinates(int x, int y)
    {
        //set up of board positions with cubic coordinates calculus
        //Z = - (X + Y), always.
        this.X = x;
        this.Y = y;
        this.Z = -(x + y);

        //set up of the world game position
        m_Height = m_Radius * 2;
        m_Width = WidthMultiplier * m_Height;
        m_Horizontal_spacing = m_Width * Multiplier;
        m_Vertical_spacing = m_Height * HeightMultiplier * Multiplier;

        float wPosX = m_Horizontal_spacing * ((float)this.X + (float)this.Y / 2);
        float wPosY = m_Vertical_spacing * (float)this.Y;
        m_Position = new Vector3(wPosX, wPosY, 0);
    }

    public Vector3 WorldPosition
    {
        get { return m_Position; }
    }

    public override string ToString()
    {
        return (X + " " + Y);
    }
}
