﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hexagon
{
    //---------------------- Member Variables ---------------------
    public Hexagon (CubicCoordinates boardPosition)
    {
        m_BoardPosition = boardPosition;
    }
    
    public CubicCoordinates m_BoardPosition;

    //---------------------- Proprieties --------------------------
    
    public CubicCoordinates BoardPosition
    {
        get { return m_BoardPosition; }
        set { m_BoardPosition = value; }
    }
}
