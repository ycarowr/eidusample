﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Games.HexPosNeg
{
    public class HexagonNegPos : Hexagon
    {
        public HexagonNegPos(CubicCoordinates boardPosition) : base(boardPosition)
        {
            
        }

        //---------------------- Member Variables ---------------------

        private int m_Value;


        //---------------------- Proprieties ---------------------------

        public IntervalValues Interval
        {
            set
            {
                Vector2 v2Interval = value.Value;
                int randomNumber = Random.Range((int)v2Interval.x, (int)v2Interval.y);
                Value = randomNumber;
            }
        }

        public int Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }
    }
}
