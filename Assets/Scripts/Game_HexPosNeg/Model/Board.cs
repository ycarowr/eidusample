﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Games.HexPosNeg;
public class Board
{
    //-------------- Members ----------------
    public List<HexagonNegPos> Hexagons { get; private set; }

    //-------------- Methods ----------------
    public Board()
    {
        Hexagons = new List<HexagonNegPos>();
    }

    public void AddHexagon(HexagonNegPos hex)
    {
        Hexagons.Add(hex);
    }

    public void RemoveHexagon(HexagonNegPos hex)
    {
        Hexagons.Remove(hex);
    }

    public void Clear()
    {
        Hexagons.Clear();
    }

}
