﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Games.HexPosNeg
{
    public class Game
    {
        //-------------- Member Variables ----------------
        public delegate void FinishGameDelegate();
        public delegate void StartGameDelegate(Board board);
        public delegate void Initialize(Board board);
        public BoardSize[] Levels;
        public Board m_Board;
        public Score m_Score;
        public IntervalValues m_InternalValues;
        public StartGameDelegate StartGame;
        public FinishGameDelegate FinishGame;

        //-------------- Methods ----------------
        public Game(BoardSize size, IntervalValues internalValues, IntVariable maxScore)
        {
            m_InternalValues = internalValues;

            m_Board = new Board();
            m_Score = new Score(maxScore);

            for (int columns = 0; columns < size.Columns; ++columns)
            {
                for (int rows = 0; rows < size.Rows; ++rows)
                {
                    //Create Board
                    CubicCoordinates boardPosition = new CubicCoordinates(columns, rows);
                    HexagonNegPos hexagon = new HexagonNegPos(boardPosition);
                    m_Board.AddHexagon(hexagon);
                }
            }
        }

        public void StartGameInvoke()
        {
            foreach (HexagonNegPos hex in m_Board.Hexagons)
            {
                hex.Interval = m_InternalValues;
            }

            if (StartGame != null)
            {
                StartGame(m_Board);
            }
        }

        public void ChooseHexagon(HexagonNegPos hex)
        {
            m_Score.Increase = hex.Value;
            if (m_Score.Value >= m_Score.MaxScore)
            {
                FinishGameInvoke();
            }
        }

        public void FinishGameInvoke()
        {
            if (FinishGame != null)
            {
                FinishGame();
            }
            m_Score.Value = 0;
        }
    }
}
