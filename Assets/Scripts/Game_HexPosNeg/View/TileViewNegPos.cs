﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

namespace Games.HexPosNeg
{
    public class TileViewNegPos : TileView
    {
        //---------------------- Member Variables ---------------------
        [SerializeField] private TextMeshPro m_ValueText;
        [SerializeField] private ColorVariable m_ColorPositive;
        [SerializeField] private ColorVariable m_ColorNegative;
        [SerializeField] private HexagonNegPos m_Model;
        [SerializeField] private Animator m_Animator;
        private string m_AnimationAppearName = "HexagonAppear";
        private string m_AnimationHideName = "HexagonHide";

        public delegate void OnButtonPressedDelegate(HexagonNegPos tile);
        public OnButtonPressedDelegate OnButtonPressed;

        //---------------------- Proprieties ---------------------
        public HexagonNegPos Hexagon
        {
            get { return m_Model; }
            set
            {
                m_Model = value;
                m_ValueText.text = m_Model.Value.ToString();
                if (m_Model.Value > 0)
                {
                    Color = m_ColorPositive.Value;
                }
                else
                {
                    Color = m_ColorNegative.Value;
                }
            }
        }

        public Color Color
        {
            get
            {
                return m_Background.color;
            }

            private set
            {
                m_Background.color = value;
            }
        }

        //---------------------- Methods ---------------------

        public override void Appear()
        {
            m_Animator.Play(m_AnimationAppearName);
        }

        public override void Hide()
        {
            m_Animator.Play(m_AnimationHideName);
        }


        public override void OnButtonPressedInvoke()
        {
            if (OnButtonPressed != null)
            {
                OnButtonPressed(m_Model);
            }
            Hide();
        }
    }
}