﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Games.HexPosNeg
{
    public class BoardView : MonoBehaviour
    {
        //---------------------- Member Variables ---------------------
        [SerializeField] private Transform m_BoardTransform;
        [SerializeField] private IntervalValues m_HexIntervalValues;
        [SerializeField] private GameObject m_TileViewPrefab;
        [SerializeField] private List<TileViewNegPos> m_Views = new List<TileViewNegPos>();

        //----------------------  Methods ----------------------------

        public List<TileViewNegPos> Views
        {
            get { return m_Views; }
        }

        public void Initialize(BoardSize boardSize)
        {

            for (int columns = 0; columns < boardSize.Columns; ++columns)
            {
                for (int rows = 0; rows < boardSize.Rows; ++rows)
                {
                    //Create Views
                    GameObject tileViewObject = Instantiate(m_TileViewPrefab, m_BoardTransform);
                    tileViewObject.name = "TileView_" + columns + "_" + rows;

                    TileViewNegPos tileViewComponent = tileViewObject.GetComponentInChildren<TileViewNegPos>();
                    m_Views.Add(tileViewComponent);
                }
            }
        }

        public void StartGame(Board board)
        {
            float animationDelay = 0;
            float delayAmount = 0.13f;
            for (int i = 0; i < m_Views.Count; ++i)
            {
                animationDelay += delayAmount;
                TileViewNegPos view = m_Views[i];
                view.Hexagon = board.Hexagons[i];
                view.transform.localPosition = board.Hexagons[i].BoardPosition.WorldPosition;
                StartCoroutine(AnimateView(view, animationDelay));
            }
        }


        public void FinishGame()
        {
            foreach (TileViewNegPos view in m_Views)
            {
                view.Hide();
            }
        }

        private IEnumerator AnimateView(TileView view, float time)
        {
            yield return new WaitForSeconds(time);
            view.Appear();
        }


    }
}
