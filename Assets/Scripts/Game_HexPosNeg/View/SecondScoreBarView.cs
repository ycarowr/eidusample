﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Games.HexPosNeg;

public class SecondScoreBarView : MonoBehaviour
{
    //----------------------  Members ----------------------------
    [SerializeField] private ScoreView m_ScoreController;
    [SerializeField] private RectTransform m_Fill;
    [SerializeField] private RectTransform m_RectTransform;

    //----------------------  Methods ----------------------------

    public void Start()
    {
        ChangeSize();
        m_ScoreController.OnBarChange += ChangeSize;
    }

    public void ChangeSize()
    {
        Vector2 newsize = m_RectTransform.sizeDelta;
        newsize.x = m_Fill.sizeDelta.x;
        m_RectTransform.sizeDelta = newsize;
        newsize = m_RectTransform.anchorMax;
        newsize.x = m_Fill.anchorMax.x;
        m_RectTransform.anchorMax = newsize;
        newsize = m_RectTransform.anchorMin;
        newsize.x = m_Fill.anchorMin.x;
        m_RectTransform.anchorMin = newsize;
    }

}
