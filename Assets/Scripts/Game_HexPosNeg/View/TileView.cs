﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public abstract class TileView : MonoBehaviour
{
    //----------------------  Members ----------------------------
    [SerializeField] protected SpriteRenderer m_Background;
    [SerializeField] protected CubicCoordinates m_BoardPosition;

    //----------------------  Methods ----------------------------
    public abstract void OnButtonPressedInvoke();
    public abstract void Appear();
    public abstract void Hide();
}
