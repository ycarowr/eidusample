﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Games.HexPosNeg
{
    public class ScoreView : MonoBehaviour
    {
        //------------------------------------ Members and Unity Callbacks ----------------------------------
        public delegate void BarChangedDelegate();

        [SerializeField] private Slider m_Slider;
        [SerializeField] private IntVariable m_MaxScore;

        public BarChangedDelegate OnBarChange;


        //------------------------------------ Event Subscriptions ------------------------------------

        public void StartGame(Board board)
        {
            m_Slider.maxValue = m_MaxScore.Value;
            m_Slider.value = m_MaxScore.Value;
            UpdateBar();
        }

        public void OnScoreChange(int value)
        {
            m_Slider.value -= value;
            UpdateBar();
        }

        private void UpdateBar()
        {
            if (OnBarChange != null)
            {
                OnBarChange();
            }
        }
    }

}
