﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class TileViewButton : MonoBehaviour
{
    public TileView m_MyView;

    private void OnMouseClickObject()
    {
        m_MyView.OnButtonPressedInvoke();
    }
}

