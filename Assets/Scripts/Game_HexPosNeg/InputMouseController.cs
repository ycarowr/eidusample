﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
/*
    Reused component from other project
 */
public class InputMouseController : MonoBehaviour
{
    //############################################################## Members and Unity Callbacks #############################################################################

    [SerializeField] private bool m_IsLocked;
    [SerializeField] private Camera m_MainCamera;
    private string m_MethodCalled = "OnMouseClickObject";


    public bool IsLocked
    {
        get { return m_IsLocked; }
        private set { m_IsLocked = value; }
    }


    private void Update()
    {
        if (!IsLocked)
        {
            CheckInputUP();
        }
    }

    //################################## Public Interface ########################

    public void Lock(float timeLocked)
    {
        IsLocked = true;
        Invoke("Unlock", timeLocked);
    }

    public void Lock()
    {
        IsLocked = true;
    }

    public void Unlock()
    {
        IsLocked = false;
    }

    //############################################################## Internal Methods #############################################################################


    private void CheckInputUP()
    {
#if UNITY_EDITOR || UNITY_WEBGL
        HandleMouseUP();
#else
        HandleTouchUP();
#endif
    }

    private void HandleTouchUP()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Ended)
            {
                OnInputUP(touch.position);
            }
        }
    }

    private void HandleMouseUP()
    {
        if (Input.GetMouseButtonUp(0))
        {
            OnInputUP(Input.mousePosition);
        }
    }

    private void OnInputUP(Vector2 inputPosition)
    {
        RaycastHit2D hitInfo2d;
        hitInfo2d = SendRaycast(inputPosition);
        if (hitInfo2d.collider != null)
        {
            GameObject colliderObject = hitInfo2d.collider.gameObject;
            colliderObject.SendMessage(m_MethodCalled, SendMessageOptions.DontRequireReceiver);
        }
    }

    private RaycastHit2D SendRaycast(Vector2 position)
    {
        Vector2 worldPoint = m_MainCamera.ScreenToWorldPoint(position);
        RaycastHit2D hitInfo2d = Physics2D.Raycast(worldPoint, m_MainCamera.transform.forward);
        return hitInfo2d;
    }

}
