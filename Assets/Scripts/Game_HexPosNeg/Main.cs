﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Games.HexPosNeg
{
    public class Main : MonoBehaviour
    {

        //----------------------  Members ----------------------------
        [SerializeField] private float TimeToStart = 1;
        [SerializeField] private BoardView m_BoardController;
        [SerializeField] private ScoreView m_ScoreController;
        [SerializeField] private BoardSize[] m_BoardSizes;
        [SerializeField] private int m_IndexBoard;
        [SerializeField] private IntervalValues m_IntervalValues;
        [SerializeField] private IntVariable m_MaxScore;
        [SerializeField] private float TimeUntilReplay = 2;
        private Game m_Game;


        //----------------------  Methods ----------------------------

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(TimeToStart);
            StartGame();
        }

        private void StartGame()
        {
            m_Game = new Game(m_BoardSizes[m_IndexBoard], m_IntervalValues, m_MaxScore);

            m_Game.StartGame += m_BoardController.StartGame;
            m_Game.StartGame += m_ScoreController.StartGame;
            m_Game.FinishGame += m_BoardController.FinishGame;
            m_Game.FinishGame += FinishGame;
            m_Game.m_Score.OnScoreChange += m_ScoreController.OnScoreChange;


            m_BoardController.Initialize(m_BoardSizes[m_IndexBoard]);


            foreach (TileViewNegPos view in m_BoardController.Views)
            {
                view.OnButtonPressed += m_Game.ChooseHexagon;
            }
            m_Game.StartGameInvoke();
        }

        private void FinishGame()
        {
            ++m_IndexBoard;
            if (m_IndexBoard > m_BoardSizes.Length - 1)
            {
                m_IndexBoard = 0;
            }

            m_BoardController.Views.Clear();
            Invoke("StartGame", TimeUntilReplay);
        }
    }
}